﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HvCommerce.Core.ApplicationServices
{
    public class HvModule
    {
        public string Name { get; set; }

        public string AssemblyName { get; set; }
    }
}
